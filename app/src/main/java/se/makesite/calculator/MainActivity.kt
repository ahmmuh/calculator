package se.makesite.calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import se.stringCalculator

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        updateUI(mainUIString = "")
    }

    val operationList : MutableList<String> = arrayListOf()
    val numCache : MutableList<String> = arrayListOf()

    private fun extraString (items: List<String>, connect: String = ""): String{

        if (items.isEmpty()) return ""
        return items.reduce{acc, s -> acc + connect + s}
    }

    private fun updateUI(mainUIString: String){

        val calculationString = extraString(operationList, connect = "")
        var calcuationTXTView = findViewById<View>(R.id.displayNumber) as TextView

        calcuationTXTView.text = calculationString
        val ans = findViewById<View>(R.id.display) as TextView
        ans.text = mainUIString


    }

    fun numberSmash (view: View){
        val button = view as Button
        val numString = button.text
        numCache.add(numString.toString())

        val text = extraString(numCache)
        updateUI(text)
    }

    fun operationSmash(view: View){
        val button = view as Button
        if (numCache.isEmpty()) return

        operationList.add(extraString(numCache))

        numCache.clear()
        operationList.add(button.text.toString())
        updateUI(button.text.toString())
    }

    private fun clearCashe(){
        operationList.clear()
        numCache.clear()
    }
    fun allClear(view: View){
        clearCashe()
        updateUI("")
    }

    fun equalSmash(view: View){
        operationList.add(extraString((numCache)))
        numCache.clear()
        val calculator = stringCalculator()
        val answer = calculator.calculate(operationList)
        updateUI("" + answer.toString())
        clearCashe()
    }
}
